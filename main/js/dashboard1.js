/*
Template Name: Admin Press Admin
Author: Themedesigner
Email: niravjoshi87@gmail.com
File: js
*/
$(function() {
    "use strict";
    // ============================================================== 
    // Sales overview
    // ============================================================== 
    Morris.Area({
        element: 'earning',
        data: [{
                period: 'Jan/18',
                Instalações: 50,
                Deleções: 80,
                Ativos: 20
            }, {
                period: 'Fev/18',
                Instalações: 130,
                Deleções: 100,
                Ativos: 80
            }, {
                period: 'Mar/18',
                Instalações: 80,
                Deleções: 60,
                Ativos: 70
            }, {
                period: 'Abr/18',
                Instalações: 70,
                Deleções: 200,
                Ativos: 140
            }, {
                period: 'Jun/18',
                Instalações: 180,
                Deleções: 150,
                Ativos: 140
            }, {
                period: 'Jul/18',
                Instalações: 105,
                Deleções: 100,
                Ativos: 80
            },
            {
                period: 'Ago/18',
                Sales: 250,
                Deleções: 150,
                Marketing: 200
            }
        ],
        xkey: 'period',
        ykeys: ['Instalações', 'Deleções', 'Ativos'],
        labels: ['Instalações', 'Deleções', 'Ativos'],
        pointSize: 3,
        fillOpacity: 0,
        pointStrokeColors: ['#6ad645', '#c1c1c1', '#1976d2'],
        behaveLikeLine: true,
        gridLineColor: '#e0e0e0',
        lineWidth: 3,
        hideHover: 'auto',
        lineColors: ['#6ad645', '#c1c1c1', '#1976d2'],
        resize: true

    });

    // ============================================================== 
    // Sales overview
    // ==============================================================
    // ============================================================== 
    // Download count
    // ============================================================== 
    var sparklineLogin = function() {
        $('.spark-count').sparkline([4, 5, 0, 10, 9, 12, 4, 9, 4, 5, 3, 10, 9, 12, 10, 9], {
            type: 'bar',
            width: '100%',
            height: '70',
            barWidth: '2',
            resize: true,
            barSpacing: '6',
            barColor: 'rgba(255, 255, 255, 0.3)'
        });

        $('.spark-count2').sparkline([20, 40, 30], {
            type: 'pie',
            height: '90',
            resize: true,
            sliceColors: ['#1cadbf', '#1f5f67', '#ffffff']
        });
    }
    var sparkResize;

    sparklineLogin();


});